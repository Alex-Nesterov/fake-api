define({ "api": [
  {
    "type": "post",
    "url": "/robots/add/",
    "title": "Create Robot",
    "name": "CreateRobot",
    "group": "Robot",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"name\":\"C-3PO\",\n    \"type\":\"droid\",\n    \"year\":\"1977\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"status\":\"OK\",\n      \"data\":\n          {\n             \"name\":\"C-3PO5\",\n             \"type\":\"droid\",\n             \"year\":1978,\n             \"id\":\"2205\"\n         }\n  }",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the Robot.</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongRequest",
            "description": "<p>Wrong request format.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\n   {\n     \"status\":\"ERROR\",\n     \"messages\":[\"name is required\",\"type is required\",\"year is required\"]\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  },
  {
    "type": "get",
    "url": "/robots/delete/:id",
    "title": "Delete Robot",
    "name": "DeleteRobot",
    "group": "Robot",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Robots unique ID.</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongRequest",
            "description": "<p>Robot not exists.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  },
  {
    "type": "get",
    "url": "/robots/:id",
    "title": "Request Robot information",
    "name": "GetRobot",
    "group": "Robot",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Robots unique ID.</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n          \"id\": \"1\",\n          \"name\": \"ASIMO\",\n          \"type\": \"humanoid\",\n          \"year\": \"2000\"\n  }",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the Robot.</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "RobotNotFound",
            "description": "<p>The id of the Robot was not found.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  },
  {
    "type": "get",
    "url": "/robots/",
    "title": "Request All robots",
    "name": "GetRobots",
    "group": "Robot",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n      {\n          \"id\": \"2\",\n          \"name\": \"ASIMO\",\n          \"type\": \"humanoid\",\n          \"year\": \"2000\"\n      },\n      {\n          \"id\": \"1\",\n          \"name\": \"C-3PO\",\n          \"type\": \"droid\",\n          \"year\": \"1977\"\n      }\n  ]",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the Robot.</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "RobotNotFound",
            "description": "<p>Robots was not found.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  },
  {
    "type": "get",
    "url": "/robots/search/:name",
    "title": "Search Robot",
    "name": "SearchRobot",
    "group": "Robot",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>string</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Robots name.</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  [\n     {\n     \"status\":\"FOUND\",\n     \"data\":\n         {\n             \"id\":\"1\",\"name\":\"C-3PO\",\"type\":\"droid\",\"year\":\"1977\"\n         }\n     }\n  ]",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the Robot.</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "RobotNotFound",
            "description": "<p>The name of the Robot was not found.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n   {\n           \"status\":\"NOT-FOUND\"\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  },
  {
    "type": "post",
    "url": "/robots/update/:id",
    "title": "Update Robot",
    "name": "UpdateRobot",
    "group": "Robot",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Robots unique ID.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"name\":\"C-3PO\",\n    \"type\":\"droid\",\n    \"year\":\"1977\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n          \"status\": \"OK\"\n  }",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "id",
            "description": "<p>Id of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "type",
            "description": "<p>Type of the Robot.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the Robot.</p> "
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongRequest",
            "description": "<p>Wrong request format.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 Conflict\n   {\n     \"status\":\"ERROR\",\n     \"messages\":[\"name is required\",\"type is required\",\"year is required\"]\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./index.php",
    "groupTitle": "Robot"
  }
] });