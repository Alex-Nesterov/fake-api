define({
  "name": "Fake API",
  "version": "0.1.0",
  "description": "Implemented Basic CRUD Functionality<br>git repository with POSTMAN collection <a href='https://bitbucket.org/Alex-Nesterov/fake-api' target='_blank'> here</a>",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2015-05-27T19:09:16.306Z",
    "url": "http://apidocjs.com",
    "version": "0.13.1"
  }
});